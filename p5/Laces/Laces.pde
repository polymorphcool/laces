// adapt these values to generate other shapes //<>//

static final int DIR_LENGTH = 2;
static final int DIR_GAP = 1;
static final float INIT_LENGTH = 30;
static final float INIT_GAP = 15;
static final int SWITCH_RANGE[] = new int[] { 1, 3 };
static final int SEGMENT_MAX = 30;

float length;
float gap;
int dir[];
int segment_count;
int switch_count;

ArrayList<PVector> pts;

AABB bbox;

boolean last_is_gap;
boolean keepon;

PVector center_current;
PVector center_target;

void setup() {

  size( 600, 600, P3D );
  noFill();

  reset();
  
}

void reset() {
  
  segment_count = 0;
  
  length = INIT_LENGTH;
  gap = INIT_GAP;
  
  keepon = true;
  last_is_gap = false;
  
  dir = new int[] { DIR_LENGTH, DIR_GAP };

  bbox = new AABB();
  center_current = new PVector();
  center_target = new PVector();

  pts = new ArrayList<PVector>();
  pts.add( new PVector(0, 0, 0) );
  
  randomise_switch();

}

void randomise_switch() {
  switch_count = int( random( SWITCH_RANGE[0], SWITCH_RANGE[1] + 1 ) );
}

void add_point() {

  int len = pts.size();

  if ( SEGMENT_MAX > -1 && len >= SEGMENT_MAX) {
    keepon = false;
    if ( !last_is_gap ) { return; }
  }

  PVector newpt = pts.get( len - 1 ).copy();

  if ( len % 2 == 1 ) {
    if ( abs(dir[0]) == DIR_LENGTH ) {
      newpt.x += length * ( dir[0]/DIR_LENGTH );
      dir[0] *= -1;
      segment_count++;
      last_is_gap = false;
    } else {
      newpt.x += gap * ( dir[0]/DIR_GAP );
      last_is_gap = true;
    }
  } else {
    if ( abs(dir[1]) == DIR_LENGTH ) {
      newpt.y += length * ( dir[1]/DIR_LENGTH );
      dir[1] *= -1;
      segment_count++;
      last_is_gap = false;
    } else {
      newpt.y += gap * ( dir[1]/DIR_GAP );
      last_is_gap = true;
    }
  }

  if ( segment_count != 0 && segment_count % switch_count == 0 ) {
    if ( segment_count % 2 == 0 ) {
      if ( abs(dir[0]) == DIR_LENGTH ) {
        if ( keepon ) newpt.x += -gap * ( dir[1]/DIR_GAP );
        int gd = ( dir[1]/DIR_GAP ) * -1;
        dir[1] = DIR_LENGTH * ( dir[0]/DIR_LENGTH ) * -1;
        dir[0] = DIR_GAP * gd;
        length = bbox.size().y;
      } else {
        if ( keepon ) newpt.y += -gap * ( dir[0]/DIR_GAP );
        int gd = ( dir[0]/DIR_GAP ) * -1;
        dir[0] = DIR_LENGTH * ( dir[1]/DIR_LENGTH ) * -1;
        dir[1] = DIR_GAP * gd;
        length = bbox.size().x;
      }
    } else {
      if ( abs(dir[0]) == DIR_LENGTH ) {
        if ( keepon ) newpt.x += gap * ( dir[1]/DIR_GAP );
        int gd = ( dir[1]/DIR_GAP );
        dir[1] = DIR_LENGTH * ( dir[0]/DIR_LENGTH );
        dir[0] = DIR_GAP * gd;
        length = bbox.size().y;
      }    
    }
    randomise_switch();
    center_target = bbox.center();
  }
  
  pts.add( newpt );
  bbox.add( newpt );
  
}

void draw() {

  if ( frameCount % 4 == 0 && keepon ) {
    add_point();
  }
  
  PVector center_diff = new PVector( 
    center_target.x - center_current.x,
    center_target.y - center_current.y,
    center_target.z - center_current.z );
  center_diff.mult( 0.05 );
  center_current.add( center_diff );
  
  background(0);
  
  strokeWeight(1);
  stroke( 255, 50 );

  for ( int i = 0; i < width; i += INIT_GAP ) {
    line( i, 0, i, height );
  }
  for ( int i = 0; i < height; i += INIT_GAP ) {
    line( 0, i, width, i );
  }

  strokeWeight(2);

  pushMatrix();
  translate( width/2 - center_current.x, height/2 - center_current.y, -center_current.z );

  int len = pts.size();
  PVector lastpt = pts.get(0);
  stroke(255);
  for ( int i = 1; i < len; ++i ) {
    PVector pt = pts.get(i);
    line( lastpt.x, lastpt.y, lastpt.z, pt.x, pt.y, pt.z );
    lastpt = pt;
  }

  //stroke(255, 100);
  //bbox.draw();

  popMatrix();

  text( 
    "segments: " + segment_count + "/"  + switch_count + 
    "\nrange:[ " + SWITCH_RANGE[0] + ", " + SWITCH_RANGE[1] + " ]" + 
    "\ninit length: " + INIT_LENGTH + 
    "\ninit gap: " + INIT_GAP 
    , 10, 25 );
    
}

void keyPressed() {
  
  reset();
  
}
