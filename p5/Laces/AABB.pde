public class AABB {
  
  private PVector _min;
  private PVector _max;
  private PVector _size;
  
  public AABB() {
    reset();
  }
  
  public void reset() {
    _min = new PVector();
    _max = new PVector();
    _size = new PVector();
  }
  
  public void add( PVector pt ) {
    if ( _min.x > pt.x ) _min.x = pt.x;
    if ( _min.y > pt.y ) _min.y = pt.y;
    if ( _min.z > pt.z ) _min.z = pt.z;
    if ( _max.x < pt.x ) _max.x = pt.x;
    if ( _max.y < pt.y ) _max.y = pt.y;
    if ( _max.z < pt.z ) _max.z = pt.z;
    _size.x = _max.x - _min.x;
    _size.y = _max.y - _min.y;
    _size.z = _max.z - _min.z;
  }
  
  public PVector size() {
    return _size;
  }
  
  public PVector center() {
    return new PVector(
      _min.x + _size.x * 0.5,
      _min.y + _size.y * 0.5,
      _min.z + _size.z * 0.5
    );
  }
  
  public void draw() {
    rect( _min.x, _min.y, _size.x, _size.y );
  }
  
}
