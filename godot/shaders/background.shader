shader_type canvas_item;
render_mode blend_mix;

uniform vec2 uv_size;
uniform vec2 uv_offset;

void fragment(){
	vec2 repeat_uv = ( UV * uv_size ) + uv_offset;
	repeat_uv.x -= float(int( repeat_uv.x ));
	repeat_uv.y -= float(int( repeat_uv.y ));
	COLOR = texture( TEXTURE, repeat_uv );
}