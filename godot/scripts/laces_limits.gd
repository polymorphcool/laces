extends Node2D

func is_active():
	return $width.visible || $height.visible

func set_width_max( v ):
	
	$width.visible = v > 0
	if v > 1:
		var vpy = get_viewport().size.y
		$width/min.clear_points()
		$width/min.add_point( Vector2( -v*0.5, -vpy ) )
		$width/min.add_point( Vector2( -v*0.5, vpy ) )
		$width/max.clear_points()
		$width/max.add_point( Vector2( v*0.5, -vpy ) )
		$width/max.add_point( Vector2( v*0.5, vpy ) )

func set_height_max( v ):
	
	$height.visible = v > 0
	if v > 1:
		var vpx = get_viewport().size.x
		$height/min.clear_points()
		$height/min.add_point( Vector2( -vpx, -v*0.5 ) )
		$height/min.add_point( Vector2( vpx, -v*0.5 ) )
		$height/max.clear_points()
		$height/max.add_point( Vector2( -vpx, v*0.5 ) )
		$height/max.add_point( Vector2( vpx, v*0.5 ) )

func _ready():
	$width.visible = false
	$height.visible = false
