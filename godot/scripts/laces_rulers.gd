extends Node2D

onready var glob = get_node( "/root/laces_globals" )

onready var display = glob.LACE_RULER_SIMPLE

func set_display( d ):
	display = d
	if display == glob.LACE_RULER_NONE and visible:
		visible = false
	elif display != glob.LACE_RULER_NONE and not visible:
		visible = true
	update()

func update():
	
	if not visible:
		return
	
	var bbox = get_parent().bbox
	var gap = get_parent().init_gap
	var strokew = get_parent().width
	
	$width.position.x = bbox.center.x
	$width.position.y = int( bbox.max.y + gap * 2 )
	$width/min.position.x = -(bbox.size.x+strokew-1) * 0.5
	$width/max.position.x = (bbox.size.x+strokew-1) * 0.5
	$width/label.rect_position.y = -12
	$width/bar.clear_points()

	$height.position.x = bbox.max.x + gap * 2
	var iy = int(bbox.center.y)
	$height.position.y = iy
	$height/min.position.y = -(bbox.size.y+strokew-1) * 0.5 + ( bbox.center.y - iy )
	$height/max.position.y = (bbox.size.y+strokew-1) * 0.5 + ( bbox.center.y - iy )
	$height/label.rect_position.x = -gap * 0.5
	$height/bar.clear_points()
	
	if display == glob.LACE_RULER_FULL:
		
		$width/bar.add_point( $width/min.position )
		$width/bar.add_point( $width/max.position )
		
		$height/bar.add_point( $height/min.position )
		$height/bar.add_point( $height/max.position )
		
		$width/label.rect_position.y = 5
		$height/label.rect_position.x = 5
		
		$width/label.text = str( bbox.size.x ) + "px (" + str( bbox.size.x + strokew ) + " w. stroke)"
		$width/label.text +=  "\n" + str( bbox.size.x * glob.to_dpi ) + 'mm'
		$width/label.text +=  " (" + str( int( 100 * (bbox.size.x + strokew) * glob.to_dpi ) * 0.01 ) + " w. stroke)"
		
		$height/label.text = str( bbox.size.y ) + "px (" + str( bbox.size.y + strokew ) + " w. stroke)"
		$height/label.text +=  "\n" + str( bbox.size.y * glob.to_dpi ) + 'mm'
		$height/label.text +=  " (" + str( int( 100 * (bbox.size.y + strokew) * glob.to_dpi ) * 0.01 ) + " w. stroke)"
		
	else:
		
		$width/label.text = str( bbox.size.x )
		$height/label.text = str( bbox.size.y )
