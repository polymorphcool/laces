# Laces

Laces is a small graphical design generator. By modifying parameters, you can produce various types of designs,
all based on a simple principle: after a given amount of U turns, the direction changes and the line continues to
make U turns until it reaches the maximum number of segments.

SVG export is functional.

![Screenshot of Laces](assets/itch-screenshot.png)

## repository structure

- `assets`: images, fonts and icons used in the project
- `godot`: standalone project to be run with Godot (3.2 and above) with UI and export function
- `p5`: early test done with processing.org

## greetings

inspirations:

- [Devine Lu Linvega](https://github.com/neauoire), especially [ronin](https://hundredrabbits.itch.io/ronin) and [dotgrid](https://hundredrabbits.itch.io/dotgrid)
- [Nervous System](https://n-e-r-v-o-u-s.com/index.php).

## credits

Laces is under GPL v.3 license. Modify it at will, share, but don't forget to share your modifications!

A [polymorph.cool](https:/polymorph.cool) object.
